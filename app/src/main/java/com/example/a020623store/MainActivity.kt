package com.example.a020623store

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.a020623store.databinding.ActivityMainBinding
import com.example.a020623store.views.ListItemFragment

class MainActivity : AppCompatActivity(), OpenFragmentInterface, Visible {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        openFragment(ListItemFragment(this))
        binding.transparentView.visibility = View.GONE
    }

    override fun openFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.open_fragment_container, fragment)
            .commit()
    }

    override fun makeVisible(int: Int) {
        when (int) {
            8 -> {
                binding.transparentView.visibility = View.GONE
            }
            0 -> {
                binding.transparentView.visibility = View.VISIBLE
            }
            4 -> {
                binding.transparentView.visibility = View.INVISIBLE
            }
        }
    }
}

interface Visible {
    fun makeVisible(int: Int)
}

interface OpenFragmentInterface {
    fun openFragment(fragment: Fragment)
}

interface OpenWindowFragmentInterface {
    fun openWindowFragment(fragment: Fragment)
    fun removeWindowFragment(fragment: Fragment)
}

interface OnCheckedChangeListener {
    fun onItemCheckedChanged(position: Int, isChecked: Boolean)
}

interface OnConfirmButtonClickListener {
    fun onClickButton(price: String)

}

interface WindowClickListener {
    fun windowClick(position: Int, isChecked: Boolean)
}
