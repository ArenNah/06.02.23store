package com.example.a020623store.model

import android.widget.ImageView
import com.example.a020623store.Type

data class MyData(
    val checkBox: Boolean,
    val title: String,
    val info: String,
    var price: Double?,
    val image: ImageView?,
    val viewType: Type
)