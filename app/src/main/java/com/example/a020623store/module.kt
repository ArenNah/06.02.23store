package com.example.a020623store

import com.example.a020623store.viewModel.OrderPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        OrderPageViewModel()
    }
}