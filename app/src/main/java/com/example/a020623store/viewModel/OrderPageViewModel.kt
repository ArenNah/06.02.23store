package com.example.a020623store.viewModel

import androidx.lifecycle.ViewModel
import com.example.a020623store.MY_DATA_LIST
import com.example.a020623store.MY_WINDOW_DATA_LIST
import com.example.a020623store.SharedPrefUtils
import com.example.a020623store.Type
import com.example.a020623store.model.MyData
import com.google.gson.Gson

class OrderPageViewModel : ViewModel() {
    private val gson = Gson()
    private val myData = ArrayList<MyData>()

    fun dataListAdd(sharedPrefUtils: SharedPrefUtils) {
        myData.add(
            MyData(
                true,
                "Choose sides (Select up to 2) (Required)",
                "",
                null,
                null,
                Type.TITLE_TYPE
            )
        )
        myData.add(
            MyData(
                true, "",
                "Add Bacon or Cheese",
                0.75,
                null,
                Type.ITEM_TYPE
            )
        )
        myData.add(
            MyData(
                true, "",
                "Add Bacon or Cheese",
                0.75,
                null,
                Type.ITEM_TYPE
            )
        )
        myData.add(
            MyData(
                true, "",
                "Add Bacon or Cheese",
                0.75,
                null,
                Type.ITEM_TYPE
            )
        )
        myData.add(
            MyData(
                true,
                "PARENT TITLE",
                "",
                null,
                null,
                Type.TITLE_TYPE
            )
        )
        myData.add(
            MyData(
                true,
                "",
                "Add Bacon or Cheese",
                0.75,
                null,
                Type.ITEM_TYPE2
            )
        )
        myData.add(
            MyData(
                true,
                "PARENT TITLE", "",
                null,
                null,
                Type.TITLE_TYPE
            )
        )
        myData.add(
            MyData(
                true,
                "Other Item",
                "Blah Blah Blah",
                null,
                null,
                Type.OTHER_ITEM
            )
        )
        myData.add(
            MyData(
                true,
                "Add Ons",
                "",
                null,
                null,
                Type.ADD_ONS_TITLE_ITEM
            )
        )
        myData.add(
            MyData(
                true,
                "Name/Title",
                "Lorem ipsum dolor sit amet, ipsum dolor sit amet,sed…",
                7.99,
                null,
                Type.ADD_ONS_WITH_IMAGE_ITEM
            )
        )
        myData.add(
            MyData(
                true,
                "",
                "",
                null,
                null,
                Type.ADD_ONS_VIEW_LINE_ITEM
            )
        )
        myData.add(
            MyData(
                true,
                "Name/Title",
                "Lorem ipsum dolor sit amet, ipsum dolor sit amet,sed…",
                7.99,
                null,
                Type.ADD_ONS_WITHOUT_IMAGE_ITEM
            )
        )
        myData.add(
            MyData(
                true,
                "Add Ons",
                "",
                null,
                null,
                Type.ADD_ONS_TITLE_ITEM
            )
        )
        myData.add(
            MyData(
                true,
                "Name/Title",
                "Lorem ipsum dolor sit amet, ipsum dolor sit amet,sed…",
                7.99,
                null,
                Type.ADD_ONS_WITH_IMAGE_BUTTON_ITEM
            )
        )

        val newJson = gson.toJson(myData)
        sharedPrefUtils.saveString(MY_DATA_LIST, newJson)
    }

    fun windowDataListAdd(sharedPrefUtils: SharedPrefUtils) {
        myData.add(
            MyData(
                true,
                "Name/Title",
                "Lorem ipsum dolor sit amet, ipsum dolor sit amet,sed…",
                7.99,
                null,
                Type.ADD_ON_WITH_IMAGE_WITHOUT_BUTTON_ITEM
            )
        )
        myData.add(
            MyData(
                true,
                "Choose sides (Select up to 2) (Required)",
                "",
                null,
                null,
                Type.TITLE_TYPE
            )
        )
        myData.add(
            MyData(
                true,
                "",
                "Add Bacon or ",
                0.75,
                null,
                Type.ITEM_TYPE_WINDOW
            )
        )
        myData.add(
            MyData(
                true,
                "",
                "Add Bacon or ",
                0.75,
                null,
                Type.ITEM_TYPE_WINDOW
            )
        )
        myData.add(
            MyData(
                true,
                "",
                "Add Bacon or ",
                0.75,
                null,
                Type.ITEM_TYPE_WINDOW
            )
        )
        myData.add(
            MyData(
                true,
                "PARENT TITLE",
                "",
                null,
                null,
                Type.TITLE_TYPE
            )
        )
        myData.add(
            MyData(
                true,
                "",
                "Add Bacon or ",
                0.75,
                null,
                Type.ITEM_TYPE2_WINDOW
            )
        )
        myData.add(
            MyData(
                true,
                "PARENT TITLE",
                "",
                null,
                null,
                Type.TITLE_TYPE
            )
        )
        myData.add(
            MyData(
                true,
                "Other Item",
                "Blah Blah Blah",
                null,
                null,
                Type.OTHER_ITEM_WINDOW
            )
        )
        val newJson = gson.toJson(myData)
        sharedPrefUtils.saveString(MY_WINDOW_DATA_LIST, newJson)
    }
}