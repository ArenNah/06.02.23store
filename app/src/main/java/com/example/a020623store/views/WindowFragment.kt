package com.example.a020623store.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a020623store.*
import com.example.a020623store.databinding.WindowFragmentBinding
import com.example.a020623store.model.MyData
import com.example.a020623store.viewModel.OrderPageViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.math.BigDecimal
import java.math.RoundingMode

class WindowFragment(
    private val removeWindowFragment: OpenWindowFragmentInterface,
    private val onConfirmButtonClickListener: OnConfirmButtonClickListener
) : Fragment(), WindowClickListener {

    private lateinit var binding: WindowFragmentBinding
    private lateinit var sharedPreferencesUtil: SharedPrefUtils
    private val orderPageViewModel = OrderPageViewModel()
    private lateinit var myData: ArrayList<MyData>
    private var price = 7.99
    private var value = 1
    private val selectedItems: MutableMap<Int, Double> = mutableMapOf()
    private lateinit var myAdapter: MyAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = WindowFragmentBinding.inflate(layoutInflater)

        sharedPreferencesUtil = SharedPrefUtils(requireContext())
        orderPageViewModel.windowDataListAdd(sharedPreferencesUtil)

        val json = sharedPreferencesUtil.getString(MY_WINDOW_DATA_LIST, EMPTY_VALUE)
        val gson = Gson()
        val type = object : TypeToken<ArrayList<MyData>>() {}.type
        myData = gson.fromJson<ArrayList<MyData>>(json, type) ?: arrayListOf()

        binding.recyclerViewWindowPage.apply {
            layoutManager = LinearLayoutManager(requireContext())
            myAdapter = MyAdapter(
                myData,
                null,
                null,
                requireContext(),
                null,
                this@WindowFragment
            )
            adapter = myAdapter
        }

        binding.configureAddCancelIcon.setOnClickListener {
            removeWindowFragment.removeWindowFragment(this)
        }

        updateValue()
        updateTotalPrice()

        binding.configureAddQuantityMin.setOnClickListener {
            if (binding.configureAddQuantity.text.toString().toInt() != 0) {
                value--
                updateValue()
                updateTotalPrice()
            }
        }
        binding.configureAddQuantityMax.setOnClickListener {
            value++
            updateValue()
            updateTotalPrice()
        }

        binding.configureAddConfirmButton.setOnClickListener {
            val newPrice = sharedPreferencesUtil.getString(WINDOW_HEADER_PRICE, EMPTY_VALUE)
            onConfirmButtonClickListener.onClickButton("$newPrice")
            removeWindowFragment.removeWindowFragment(this)
        }
        return binding.root
    }

    private fun updateValue() {
        binding.configureAddQuantity.text = value.toString()
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    private fun updateTotalPrice() {
        var summary = 0.0
        for ((position, price) in selectedItems) {
            summary += price
        }

        val basedPrice = value * price
        val totalPrice = basedPrice + summary
        val roundedPrice = BigDecimal(totalPrice).setScale(2, RoundingMode.HALF_UP)
        for (i in 0 until myData.size) {
            if (myData[i].viewType == Type.ADD_ON_WITH_IMAGE_WITHOUT_BUTTON_ITEM) {
                myData[i].price = roundedPrice.toDouble()
                sharedPreferencesUtil.saveString(WINDOW_HEADER_PRICE, roundedPrice.toString())
            }
        }
        myAdapter.notifyDataSetChanged()

    }

    override fun windowClick(position: Int, isChecked: Boolean) {
        if (isChecked) {
            val itemPrice = myData[position].price
            if (itemPrice != null) {
                selectedItems[position] = itemPrice
            }
        } else {
            selectedItems.remove(position)
        }
        var summary = 0.0
        for ((position, price) in selectedItems) {
            summary += price
        }

        val basedPrice = value * price
        val totalPrice = basedPrice + summary
        val roundedPrice = BigDecimal(totalPrice).setScale(2, RoundingMode.HALF_UP)
        for (i in 0 until myData.size) {
            if (myData[i].viewType == Type.ADD_ON_WITH_IMAGE_WITHOUT_BUTTON_ITEM) {
                myData[i].price = roundedPrice.toDouble()
            }
        }
    }
}