package com.example.a020623store.views

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a020623store.*
import com.example.a020623store.databinding.ListMainFragmentBinding
import com.example.a020623store.model.MyData
import com.example.a020623store.viewModel.OrderPageViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.math.BigDecimal
import java.math.RoundingMode

class ListItemFragment(private val visibility: Visible) : Fragment(), OpenWindowFragmentInterface,
    OnCheckedChangeListener, OnConfirmButtonClickListener {

    private lateinit var binding: ListMainFragmentBinding
    private lateinit var openFragment: OpenFragmentInterface
    private lateinit var sharedPreferencesUtil: SharedPrefUtils
    private val orderPageViewModel = OrderPageViewModel()
    private lateinit var myData: ArrayList<MyData>
    private lateinit var myDataWindow: ArrayList<MyData>
    private var value = 1
    private var price = 3.28
    private val selectedItems: MutableMap<Int, Double> = mutableMapOf()
    private lateinit var myAdapter: MyAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OpenFragmentInterface) {
            openFragment = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ListMainFragmentBinding.inflate(layoutInflater)

        sharedPreferencesUtil = SharedPrefUtils(requireContext())
        orderPageViewModel.dataListAdd(sharedPreferencesUtil)

        val sharedPrefUtils = SharedPrefUtils(requireContext())
        val json = sharedPrefUtils.getString(MY_DATA_LIST, EMPTY_VALUE)
        val gson = Gson()
        val type = object : TypeToken<ArrayList<MyData>>() {}.type
        myData = gson.fromJson<ArrayList<MyData>>(json, type) ?: arrayListOf()

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            myAdapter = MyAdapter(
                myData,
                this@ListItemFragment,
                this@ListItemFragment,
                requireContext(),
                this@ListItemFragment,
                null
            )
            adapter = myAdapter
        }

        value = binding.quantity.text.toString().toInt()
        price = binding.price.text.toString().replace("$", "").toDouble()
        updateValue()
        updateTotalPrice()

        binding.quantityMax.setOnClickListener {
            value++
            updateValue()
            updateTotalPrice()
        }

        binding.quantityMin.setOnClickListener {
            if (binding.quantity.text.toString().toInt() != 0) {
                value--
                updateValue()
                updateTotalPrice()
            }
        }
        return binding.root
    }

    override fun openWindowFragment(fragment: Fragment) {
        parentFragmentManager.beginTransaction()
            .add(R.id.open_window_fragment, fragment)
            .commit()
        visibility.makeVisible(View.VISIBLE)
    }

    override fun removeWindowFragment(fragment: Fragment) {
        parentFragmentManager.beginTransaction()
            .remove(fragment)
            .commit()
        visibility.makeVisible(View.GONE)
    }

    private fun updateValue() {
        binding.quantity.text = value.toString()
    }

    @SuppressLint("SetTextI18n")
    private fun updateTotalPrice() {
        var summary = 0.0
        for ((position, price) in selectedItems) {
            summary += price
        }
        val basedPrice = value * price
        val totalPrice = basedPrice + summary
        val roundedPrice = BigDecimal(totalPrice).setScale(2, RoundingMode.HALF_UP)
        binding.price.text = "$$roundedPrice"
    }

    @SuppressLint("SetTextI18n")
    override fun onItemCheckedChanged(position: Int, isChecked: Boolean) {
        if (isChecked) {
            val itemPrice = myData[position].price
            if (itemPrice != null) {
                selectedItems[position] = itemPrice
            }
        } else {
            selectedItems.remove(position)
        }

        var summary = 0.0
        for ((position, price) in selectedItems) {
            summary += price
        }
        val basedPrice = value * price
        val totalPrice = basedPrice + summary
        val roundedPrice = BigDecimal(totalPrice).setScale(2, RoundingMode.HALF_UP)
        binding.price.text = "$$roundedPrice"
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onClickButton(price: String) {
        for (i in 0 until myData.size) {
            if (myData[i].viewType == Type.ADD_ONS_WITH_IMAGE_BUTTON_ITEM) {
                myData[i].price = price.toDouble()
            }
        }
        myAdapter.setStringList(myData)
        myAdapter.notifyDataSetChanged()
    }
}