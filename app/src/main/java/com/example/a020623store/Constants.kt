package com.example.a020623store

const val MY_USER_PREF = "MyUserPref"
const val MY_WINDOW_DATA_LIST = "my_window_data_list"
const val MY_DATA_LIST = "my_data_list"
const val EMPTY_VALUE = ""
const val WINDOW_HEADER_PRICE = "window_header_price"
