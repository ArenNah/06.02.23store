package com.example.a020623store


import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.a020623store.model.MyData
import com.example.a020623store.views.WindowFragment
import java.math.BigDecimal
import java.math.RoundingMode

class MyAdapter(
    private var myData: ArrayList<MyData>,
    private val openWindowFragment: OpenWindowFragmentInterface?,
    private val onCheckedChangeListener: OnCheckedChangeListener?,
    private val context: Context,
    private val confirmButtonClickListener: OnConfirmButtonClickListener?,
    private val windowClickListener: WindowClickListener?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var valueWithImageViewHolder: Int = 0
    private var priceWithImageViewHolder: Double = 0.00
    private var valueWithoutImageViewHolder: Int = 0
    private var priceWithoutImageViewHolder: Double = 0.00
    private lateinit var sharedPrefUtils: SharedPrefUtils
    fun setStringList(myNewData: ArrayList<MyData>) {
        myData = myNewData
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val checkBoxItem: CheckBox
        val nameItem: TextView
        val priceItem: TextView

        init {
            checkBoxItem = itemView.findViewById(R.id.checkbox_item)
            nameItem = itemView.findViewById(R.id.name_item)
            priceItem = itemView.findViewById(R.id.price_item)
        }
    }

    inner class ViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkBoxItem2: CheckBox
        val nameItem2: TextView
        val priceItem2: TextView

        init {
            checkBoxItem2 = itemView.findViewById(R.id.checkbox_item_2)
            nameItem2 = itemView.findViewById(R.id.name_item_2)
            priceItem2 = itemView.findViewById(R.id.price_item_2)
        }
    }

    inner class ViewHolderWindow(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkBoxItemWindow: CheckBox
        val nameItemWindow: TextView
        val priceItemWindow: TextView

        init {
            checkBoxItemWindow = itemView.findViewById(R.id.checkbox_item_window)
            nameItemWindow = itemView.findViewById(R.id.name_item_window)
            priceItemWindow = itemView.findViewById(R.id.price_item_window)
        }
    }

    inner class ViewHolderWindow2(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkBoxItemWindow2: CheckBox
        val nameItemWindow2: TextView
        val priceItemWindow2: TextView

        init {
            checkBoxItemWindow2 = itemView.findViewById(R.id.checkbox_item_window_2)
            nameItemWindow2 = itemView.findViewById(R.id.name_item_window_2)
            priceItemWindow2 = itemView.findViewById(R.id.price_item_window_2)
        }
    }

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val headerTitle: TextView

        init {
            headerTitle = itemView.findViewById(R.id.header_title)
        }
    }

    inner class OtherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val otherInfoTitle: TextView
        val otherInfo: EditText

        init {
            otherInfoTitle = itemView.findViewById(R.id.other_info_Title)
            otherInfo = itemView.findViewById(R.id.other_info)
        }
    }

    inner class HeaderAddOnsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val addOnsTitle: TextView

        init {
            addOnsTitle = itemView.findViewById(R.id.add_ons_title)
        }
    }

    inner class ItemAddOnsWithImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewAddOnceWithImage: ImageView
        val nameTitleAddOnceWithImage: TextView
        val subtitleAddOnceWithImage: TextView
        val priceAddOnceWithImage: TextView
        val quantityAddOnceWithImage: TextView
        val quantityMinAddOnceWithImage: TextView
        val quantityMaxAddOnceWithImage: TextView

        init {
            imageViewAddOnceWithImage = itemView.findViewById(R.id.imageView_add_once_with_image)
            nameTitleAddOnceWithImage = itemView.findViewById(R.id.nameTitle_add_once_with_image)
            subtitleAddOnceWithImage = itemView.findViewById(R.id.subtitle_add_once_with_image)
            priceAddOnceWithImage = itemView.findViewById(R.id.price_add_once_with_image)
            quantityAddOnceWithImage = itemView.findViewById(R.id.quantity_add_once_with_image)
            quantityMinAddOnceWithImage =
                itemView.findViewById(R.id.quantity_min_add_once_with_image)
            quantityMaxAddOnceWithImage =
                itemView.findViewById(R.id.quantity_max_add_once_with_image)
        }
    }

    inner class ViewLineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val viewLineAddOns: View

        init {
            viewLineAddOns = itemView.findViewById(R.id.view_line_add_ons)
        }
    }

    inner class ItemAddOnsWithoutImageViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val nameTitleAddOnceWithoutImage: TextView
        val subtitleAddOnceWithoutImage: TextView
        val priceAddOnceWithoutImage: TextView
        val quantityAddOnceWithoutImage: TextView
        val quantityMinAddOnceWithoutImage: TextView
        val quantityMaxAddOnceWithoutImage: TextView

        init {

            nameTitleAddOnceWithoutImage =
                itemView.findViewById(R.id.nameTitle_add_once_without_image)
            subtitleAddOnceWithoutImage =
                itemView.findViewById(R.id.subtitle_add_once_without_image)
            priceAddOnceWithoutImage = itemView.findViewById(R.id.price_add_once_without_image)
            quantityAddOnceWithoutImage =
                itemView.findViewById(R.id.quantity_add_once_without_image)
            quantityMinAddOnceWithoutImage =
                itemView.findViewById(R.id.quantity_min_add_once_without_image)
            quantityMaxAddOnceWithoutImage =
                itemView.findViewById(R.id.quantity_max_add_once_without_image)
        }
    }

    inner class ItemAddOnsWithImageAndButtonViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val imageViewAddOnceWithButton: ImageView
        val nameTitleAddOnceWithButton: TextView
        val subtitleAddOnceWithButton: TextView
        val priceAddOnceWithButton: TextView
        val configureButtonAddOnceWithButton: TextView

        init {
            imageViewAddOnceWithButton = itemView.findViewById(R.id.imageView_add_once_with_button)
            nameTitleAddOnceWithButton = itemView.findViewById(R.id.nameTitle_add_once_with_button)
            subtitleAddOnceWithButton = itemView.findViewById(R.id.subtitle_add_once_with_button)
            priceAddOnceWithButton = itemView.findViewById(R.id.price_add_once_with_button)
            configureButtonAddOnceWithButton =
                itemView.findViewById(R.id.configure_button_add_once_with_button)
        }
    }

    inner class ItemAddOnWithImageAndWithoutButtonViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val imageViewAddOnceWithoutButton: ImageView
        val nameTitleAddOnceWithoutButton: TextView
        val subtitleAddOnceWithoutButton: TextView
        val priceAddOnceWithoutButton: TextView

        init {
            imageViewAddOnceWithoutButton =
                itemView.findViewById(R.id.imageView_add_once_without_button)
            nameTitleAddOnceWithoutButton =
                itemView.findViewById(R.id.nameTitle_add_once_without_button)
            subtitleAddOnceWithoutButton =
                itemView.findViewById(R.id.subtitle_add_once_without_button)
            priceAddOnceWithoutButton = itemView.findViewById(R.id.price_add_once_without_button)
        }
    }

    inner class OtherWindowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val otherInfoTitleWindow: TextView
        val otherInfoWindow: EditText

        init {
            otherInfoTitleWindow = itemView.findViewById(R.id.other_info_title_window)
            otherInfoWindow = itemView.findViewById(R.id.other_info_window)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        sharedPrefUtils = SharedPrefUtils(context)
        return when (viewType) {
            Type.TITLE_TYPE.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_list_items, parent, false)
                HeaderViewHolder(view)
            }
            Type.ITEM_TYPE.value -> {
                val view =
                    LayoutInflater.from(parent.context).inflate(R.layout.item_design, parent, false)
                ViewHolder(view)
            }
            Type.ITEM_TYPE2.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_desing_2, parent, false)
                ViewHolder2(view)
            }
            Type.OTHER_ITEM.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.other_item_design, parent, false)
                OtherViewHolder(view)
            }
            Type.OTHER_ITEM_WINDOW.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.other_item_design_window, parent, false)
                OtherWindowViewHolder(view)
            }
            Type.ADD_ONS_TITLE_ITEM.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_add_once, parent, false)
                HeaderAddOnsHolder(view)
            }
            Type.ADD_ONS_WITH_IMAGE_ITEM.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_design_add_once_with_image, parent, false)
                ItemAddOnsWithImageViewHolder(view)
            }
            Type.ADD_ONS_VIEW_LINE_ITEM.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_add_once, parent, false)
                ViewLineViewHolder(view)
            }
            Type.ADD_ONS_WITHOUT_IMAGE_ITEM.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_design_add_once_without_image, parent, false)
                ItemAddOnsWithoutImageViewHolder(view)
            }
            Type.ADD_ONS_WITH_IMAGE_BUTTON_ITEM.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_add_once_with_image_and_configure_button, parent, false)
                ItemAddOnsWithImageAndButtonViewHolder(view)
            }
            Type.ADD_ON_WITH_IMAGE_WITHOUT_BUTTON_ITEM.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_design_add_on_with_image_without_button, parent, false)
                ItemAddOnWithImageAndWithoutButtonViewHolder(view)
            }
            Type.ITEM_TYPE_WINDOW.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_design_window, parent, false)
                ViewHolderWindow(view)
            }
            Type.ITEM_TYPE2_WINDOW.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_design_window_2, parent, false)
                ViewHolderWindow2(view)
            }
            else -> throw IllegalArgumentException("Invalid viewType: $viewType")
        }
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (myData[position].viewType) {
            Type.TITLE_TYPE -> {
                (holder as HeaderViewHolder).headerTitle.text = myData[position].title
            }
            Type.ITEM_TYPE -> {
                (holder as ViewHolder)
                holder.nameItem.text = myData[position].info
                holder.priceItem.text = "+$${myData[position].price}"
                holder.checkBoxItem.setOnCheckedChangeListener { _, isChecked ->
                    onCheckedChangeListener?.onItemCheckedChanged(position, isChecked)
                }
            }
            Type.ITEM_TYPE2 -> {
                (holder as ViewHolder2)
                holder.nameItem2.text = myData[position].info
                holder.priceItem2.text = "+$${myData[position].price}"
                holder.checkBoxItem2.setOnCheckedChangeListener { _, isChecked ->
                    onCheckedChangeListener?.onItemCheckedChanged(position, isChecked)
                }

            }
            Type.OTHER_ITEM -> {
                (holder as OtherViewHolder).otherInfoTitle.text = myData[position].title
            }
            Type.OTHER_ITEM_WINDOW -> {
                (holder as OtherWindowViewHolder).otherInfoTitleWindow.text = myData[position].title
            }
            Type.ADD_ONS_TITLE_ITEM -> {
                (holder as HeaderAddOnsHolder).addOnsTitle.text = myData[position].title
            }
            Type.ADD_ONS_WITH_IMAGE_ITEM -> {
                (holder as ItemAddOnsWithImageViewHolder).nameTitleAddOnceWithImage.text =
                    myData[position].title
                holder.subtitleAddOnceWithImage.text = myData[position].info
                holder.priceAddOnceWithImage.text = myData[position].price.toString()
                valueWithImageViewHolder = holder.quantityAddOnceWithImage.text.toString().toInt()
                priceWithImageViewHolder = holder.priceAddOnceWithImage.text.toString().toDouble()

                updatePriceAddOnsWithImage(holder)
                updateValueAddOnsWithImage(holder)

                holder.quantityMaxAddOnceWithImage.setOnClickListener {
                    valueWithImageViewHolder++
                    updateValueAddOnsWithImage(holder)
                    updatePriceAddOnsWithImage(holder)
                }

                holder.quantityMinAddOnceWithImage.setOnClickListener {
                    valueWithImageViewHolder--
                    updateValueAddOnsWithImage(holder)
                    updatePriceAddOnsWithImage(holder)
                }
            }
            Type.ADD_ONS_VIEW_LINE_ITEM -> {
                (holder as ViewLineViewHolder)
            }
            Type.ADD_ONS_WITHOUT_IMAGE_ITEM -> {
                (holder as ItemAddOnsWithoutImageViewHolder).nameTitleAddOnceWithoutImage.text =
                    myData[position].title
                holder.subtitleAddOnceWithoutImage.text = myData[position].info
                holder.priceAddOnceWithoutImage.text = myData[position].price.toString()

                valueWithoutImageViewHolder =
                    holder.quantityAddOnceWithoutImage.text.toString().toInt()

                priceWithoutImageViewHolder =
                    holder.priceAddOnceWithoutImage.text.toString().toDouble()

                updatePriceAddOnsWithoutImage(holder)
                updateValueAddOnsWithoutImage(holder)

                holder.quantityMaxAddOnceWithoutImage.setOnClickListener {
                    valueWithoutImageViewHolder++
                    updateValueAddOnsWithoutImage(holder)
                    updatePriceAddOnsWithoutImage(holder)
                }
                holder.quantityMinAddOnceWithoutImage.setOnClickListener {
                    valueWithoutImageViewHolder--
                    updateValueAddOnsWithoutImage(holder)
                    updatePriceAddOnsWithoutImage(holder)
                }
            }
            Type.ADD_ONS_WITH_IMAGE_BUTTON_ITEM -> {
                (holder as ItemAddOnsWithImageAndButtonViewHolder).nameTitleAddOnceWithButton.text =
                    myData[position].title
                holder.subtitleAddOnceWithButton.text = myData[position].info
                holder.configureButtonAddOnceWithButton.setOnClickListener {}
                holder.priceAddOnceWithButton.text = "$${myData[position].price}"
                holder.configureButtonAddOnceWithButton.setOnClickListener {
                    openWindowFragment?.openWindowFragment(
                        WindowFragment(
                            openWindowFragment,
                            confirmButtonClickListener!!
                        )
                    )
                }
            }
            Type.ADD_ON_WITH_IMAGE_WITHOUT_BUTTON_ITEM -> {
                (holder as ItemAddOnWithImageAndWithoutButtonViewHolder).nameTitleAddOnceWithoutButton.text =
                    myData[position].title
                holder.subtitleAddOnceWithoutButton.text = myData[position].info
                holder.priceAddOnceWithoutButton.text = "$${myData[position].price}"
            }
            Type.ITEM_TYPE_WINDOW -> {
                (holder as ViewHolderWindow)
                holder.nameItemWindow.text = myData[position].info
                holder.priceItemWindow.text = "+$${myData[position].price}"
                holder.checkBoxItemWindow.setOnCheckedChangeListener { _, isChecked ->
                    windowClickListener?.windowClick(position, isChecked)
                    notifyDataSetChanged()
                }
            }
            Type.ITEM_TYPE2_WINDOW -> {
                (holder as ViewHolderWindow2)
                holder.nameItemWindow2.text = myData[position].info
                holder.priceItemWindow2.text = "+$${myData[position].price}"
                holder.checkBoxItemWindow2.setOnCheckedChangeListener { _, isChecked ->
                    windowClickListener?.windowClick(position, isChecked)
                    notifyDataSetChanged()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return myData.size
    }

    override fun getItemViewType(position: Int): Int {
        return myData[position].viewType.value
    }

    private fun updateValueAddOnsWithImage(holder: ItemAddOnsWithImageViewHolder) {
        holder.quantityAddOnceWithImage.text = valueWithImageViewHolder.toString()
    }

    @SuppressLint("SetTextI18n")
    private fun updatePriceAddOnsWithImage(holder: ItemAddOnsWithImageViewHolder) {
        val allPrice = valueWithImageViewHolder * priceWithImageViewHolder
        val roundedPrice = BigDecimal(allPrice).setScale(2, RoundingMode.HALF_UP)
        holder.priceAddOnceWithImage.text = "$$roundedPrice"
    }

    private fun updateValueAddOnsWithoutImage(holder: ItemAddOnsWithoutImageViewHolder) {
        holder.quantityAddOnceWithoutImage.text = valueWithoutImageViewHolder.toString()
    }

    @SuppressLint("SetTextI18n")
    private fun updatePriceAddOnsWithoutImage(holder: ItemAddOnsWithoutImageViewHolder) {
        val allPrice = valueWithoutImageViewHolder * priceWithoutImageViewHolder
        val roundedPrice = BigDecimal(allPrice).setScale(2, RoundingMode.HALF_UP)
        holder.priceAddOnceWithoutImage.text = "$$roundedPrice"
    }

}
